package br.com.albericocarvalho.carsystem.security;

import java.io.Serializable;

import lombok.Data;

@Data
public class AccountCredentials implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;

}
