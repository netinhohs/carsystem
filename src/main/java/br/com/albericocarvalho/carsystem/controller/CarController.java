package br.com.albericocarvalho.carsystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.albericocarvalho.carsystem.model.Car;
import br.com.albericocarvalho.carsystem.security.JwtTokenProvider;
import br.com.albericocarvalho.carsystem.service.CarService;
import io.swagger.annotations.Api;

@Api(tags = {"Cars Endpoint"})
@RestController
@RequestMapping(value = "/api/cars")
public class CarController {
	
	@Autowired
	private CarService service;
	
	@Autowired
	JwtTokenProvider tokenProvider;
	
	@GetMapping()
	public List<Car> findAll(HttpServletRequest req) {
		
		String token = tokenProvider.resolveToken(req);
		String userName = tokenProvider.getUserName(token);
		
		System.out.println(userName);
		return service.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public Car findById(HttpServletRequest req, @RequestParam("id") int id) {
		
		return service.findById(id);
	}

}
