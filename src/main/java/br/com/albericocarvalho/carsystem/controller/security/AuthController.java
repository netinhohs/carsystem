package br.com.albericocarvalho.carsystem.controller.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.ResponseEntity.ok;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.albericocarvalho.carsystem.model.security.UserAuth;
import br.com.albericocarvalho.carsystem.repository.security.UserAuthRepository;
import br.com.albericocarvalho.carsystem.security.AccountCredentials;
import br.com.albericocarvalho.carsystem.security.JwtTokenProvider;

@RestController
@RequestMapping("/api")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	JwtTokenProvider tokenProvider;
	
	@Autowired
	UserAuthRepository repository;
	
	@PostMapping(value = "/signin")
	public ResponseEntity<?> userAuthSigin(@RequestBody AccountCredentials data){
		try {
			String username = data.getUsername();
			String password = data.getPassword();
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			
			UserAuth userAuth = repository.findByUserName(username);
			String token;
			
			if (userAuth == null)
				throw new UsernameNotFoundException("Invalid login or password");
			
			token = tokenProvider.createToken(username, userAuth.getRoles());
			Map<Object, Object> model = new HashMap<Object, Object>();
			
			model.put("username", username);
			model.put("token", token);
			
			return ok(model);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadCredentialsException("Invalid login or password");
		}
		
	}

}
