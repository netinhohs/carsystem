package br.com.albericocarvalho.carsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.albericocarvalho.carsystem.model.User;
import br.com.albericocarvalho.carsystem.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = {"Users Endpoint"})
@RestController
@RequestMapping(value = "/api/users")
public class UserController {

	@Autowired
	private UserService service;
	
	@ApiOperation(value = "Find All Users")
	@GetMapping()
	public List<User> findAll(){
		return service.findAll();
	}
	
	@GetMapping(value = "/{id}")
	public User findById(@PathVariable("id") int id) {
		return service.findById(id);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable("id") int id){
		service.remove(id);
		
		return ResponseEntity.ok().build();
	}
	
	@PostMapping(value = "/{id}")
	public User create(@RequestBody User user) {
		return service.create(user);
	}
	
}
