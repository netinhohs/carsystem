package br.com.albericocarvalho.carsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.albericocarvalho.carsystem.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer>{

	@Query("SELECT u FROM User u WHERE u.login = :login")
	User findByLogin(@Param("login") String login);
}
