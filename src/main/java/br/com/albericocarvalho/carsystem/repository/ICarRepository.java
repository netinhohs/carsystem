package br.com.albericocarvalho.carsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.albericocarvalho.carsystem.model.Car;

@Repository
public interface ICarRepository extends JpaRepository<Car, Integer>{

}
