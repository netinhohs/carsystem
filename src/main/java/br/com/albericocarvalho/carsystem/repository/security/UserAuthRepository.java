package br.com.albericocarvalho.carsystem.repository.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.albericocarvalho.carsystem.model.security.UserAuth;

@Repository
public interface UserAuthRepository extends JpaRepository<UserAuth, Long> {

	@Query("SELECT u FROM UserAuth u WHERE u.userName = :userName")
	UserAuth findByUserName(@Param("userName") String userName);
}
