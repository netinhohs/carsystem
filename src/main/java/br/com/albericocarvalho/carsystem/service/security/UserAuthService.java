package br.com.albericocarvalho.carsystem.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.albericocarvalho.carsystem.model.security.UserAuth;
import br.com.albericocarvalho.carsystem.repository.security.UserAuthRepository;

@Service
public class UserAuthService implements UserDetailsService {
	
	@Autowired
	UserAuthRepository repository;
	
	public UserAuthService(UserAuthRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		UserAuth user = repository.findByUserName(userName);
		
		if(user == null)
			throw new UsernameNotFoundException("User "+ userName + " Not Found 333");
		return user;
	}

}
