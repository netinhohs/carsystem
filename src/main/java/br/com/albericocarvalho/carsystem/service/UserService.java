package br.com.albericocarvalho.carsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.albericocarvalho.carsystem.exception.NotFoundException;
import br.com.albericocarvalho.carsystem.model.User;
import br.com.albericocarvalho.carsystem.repository.IUserRepository;

@Service
public class UserService {

	@Autowired
	private IUserRepository repository;
	
	public User create(User user) {
		return repository.save(user);
	}
	
	public List<User> findAll(){
		return repository.findAll();
	}
	
	public void remove(Integer id) {
		User entity = repository.findById(id).orElseThrow( () -> new NotFoundException("USER NOT FOUND"));
		
		repository.delete(entity);
	}
	
	public User findById(Integer id) {
		return repository.findById(id).orElseThrow( () -> new NotFoundException("USER NOT FOUND"));
	}
	
	public User findByLogin(String login) {
		User user = repository.findByLogin(login);
		
		if(user == null)
			throw new NotFoundException("USER NOT FOUND");
		return user;
	}
	
	
}
