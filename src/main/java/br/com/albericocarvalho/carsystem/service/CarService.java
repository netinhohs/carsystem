package br.com.albericocarvalho.carsystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.albericocarvalho.carsystem.exception.NotFoundException;
import br.com.albericocarvalho.carsystem.model.Car;
import br.com.albericocarvalho.carsystem.repository.ICarRepository;

@Service
public class CarService {
	
	@Autowired
	private ICarRepository repository;

	
	public Car save(Car car) {
		return repository.save(car);
	}
	
	public List<Car> findAll(){
		return repository.findAll();
	}
	
	public Car findById(int id) {
		return repository.findById(id).orElseThrow( () -> new NotFoundException("USER NOT FOUND"));
	}
}
