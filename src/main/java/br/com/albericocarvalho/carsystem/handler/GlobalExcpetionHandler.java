package br.com.albericocarvalho.carsystem.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.albericocarvalho.carsystem.exception.InvalidJwtAuthenticationException;
import br.com.albericocarvalho.carsystem.exception.NotFoundException;
import br.com.albericocarvalho.carsystem.exception.model.ExceptionModel;

@ControllerAdvice
@RestController
public class GlobalExcpetionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionModel> handleGlobalExpcetion(Exception ex, WebRequest we){
		ExceptionModel exception = new ExceptionModel(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(), new Date());
		return new ResponseEntity<>(exception, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<ExceptionModel> handleNotFoundExpcetion(Exception ex, WebRequest we){
		ExceptionModel exception = new ExceptionModel(ex.getMessage(), HttpStatus.NOT_FOUND.value(), new Date());
		return new ResponseEntity<>(exception, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(InvalidJwtAuthenticationException.class)
	public final ResponseEntity<ExceptionModel> handleInvalidJwtAuthenticationException(Exception ex, WebRequest we){
		ExceptionModel exception = new ExceptionModel(ex.getMessage(), HttpStatus.BAD_REQUEST.value(), new Date());
		return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
	}

}
