package br.com.albericocarvalho.carsystem.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.albericocarvalho"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Restfull API Teste Pitang", 
				"Usuários e seus carros cadastrados",
				"V1",
				"Terms of Services",
				new Contact("Albérico Carvalho", 
						"https://www.linkedin.com/in/alb%C3%A9rico-carvalho-341ab998/", 
						"albericoneto.carvalho@gmail.com"),
				"MIT",
				"",
				Collections.emptyList());
	}
}
