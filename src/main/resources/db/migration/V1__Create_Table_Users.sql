CREATE TABLE IF NOT EXISTS `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(100) NOT NULL,
	`last_name` varchar(100) NULL,
	`email` varchar(100) NOT NULL,
	`birthday` DATE NOT NULL,
	`login` VARCHAR(100) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`phone` VARCHAR(20) NULL,
	unique (email),
	PRIMARY KEY (id)
)