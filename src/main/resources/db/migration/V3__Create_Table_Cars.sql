CREATE TABLE IF NOT EXISTS `cars` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`year` int NOT NULL,
	`license_plate` varchar(100) NOT NULL,
	`model` varchar(100) NOT NULL,
	`color` varchar(100) NOT NULL,
	`user_id` int(11) not null,
	FOREIGN KEY (user_id) REFERENCES users(id),
	PRIMARY KEY (id)
)